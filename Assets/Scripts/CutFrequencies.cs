﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioLowPassFilter))]
[RequireComponent(typeof(AudioHighPassFilter))]

public class CutFrequencies : MonoBehaviour
{
	public Instatiates512cubes instatiates512cubes;

	void Update()
	{
		VisibleSamples();
	}
	void VisibleSamples()
	{
		int firstSeenSample = 0;
		int lastSeenSample = 0;
		bool foundFirstSample = false;

		for (int i = 1; i <= instatiates512cubes._sampleParametricCube.Length; i++)
		{
			Vector3 screenPoint = Camera.main.WorldToViewportPoint(instatiates512cubes._sampleParametricCube[i - 1].transform.position);

			if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1 && foundFirstSample)
			{
				if (lastSeenSample == i - 1)
				{
					lastSeenSample++;
				}
				else
				{
					break;
				}
			}
			else if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
			{
				firstSeenSample = i - 1;
				lastSeenSample = i;
				foundFirstSample = true;
			}
		}
		
		GetComponent<AudioLowPassFilter>().cutoffFrequency = lastSeenSample * 43;
		GetComponent<AudioHighPassFilter>().cutoffFrequency = firstSeenSample * 43;
	}
}
