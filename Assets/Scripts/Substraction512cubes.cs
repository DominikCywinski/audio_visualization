﻿using UnityEngine;

public class Substraction512cubes : Instatiates512cubes
{
	public Audio _audio2;

	protected override void Update()
	{
		for (int i = 0; i < 512; i++)
		{
			if (_audio._scale[i] != 0 && _audio2._scale[i] != 0)
			{
				_sampleParametricCube[i].SetValue(_audio._samples[i] - _audio2._samples[i], Mathf.Abs(_audio._scale[i] - _audio2._scale[i]), _maxScale, GetMaxValue());
			}
			else if(_audio._scale[i] == 0)
			{
				_sampleParametricCube[i].SetValue(_audio._samples[i] - _audio2._samples[i], _audio2._scale[i], _maxScale, GetMaxValue());
			}
			else if(_audio2._scale[i] == 0)
			{
				_sampleParametricCube[i].SetValue(_audio._samples[i] - _audio2._samples[i], _audio._scale[i], _maxScale, GetMaxValue());
			}
		}
	}

	float GetMaxValue()
	{
		float temp_max = Mathf.Abs(_audio._scale[0] - _audio2._scale[0]);

		for (int i = 0; i < 512; i++)
		{
			if (Mathf.Abs(_audio._scale[0] - _audio2._scale[0]) > temp_max)
			{
				temp_max = Mathf.Abs(_audio._scale[0] - _audio2._scale[0]);
			}
		}
		
		return temp_max;
	}
}