﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyboardShortcuts : MonoBehaviour
{
	Vector3 originalPosition;
	Quaternion originalRotation;
	Quaternion originalRotation2;

	private void Start()
	{
		originalPosition = transform.localPosition;
		originalRotation = transform.localRotation;
		originalRotation2 = Camera.main.transform.localRotation;
	}

	private void Update()
	{
		GoToMenu();
		ResetPosition();
	}

	public void GoToMenu()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene(0);
		}
	}

	void ResetPosition()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			gameObject.transform.position = originalPosition;
			gameObject.transform.rotation = originalRotation;
			Camera.main.transform.rotation = originalRotation2;
		}
	}
}
