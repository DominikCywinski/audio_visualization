﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModeMenu : MonoBehaviour
{
	public void CutFrequenciesVisualization()
	{
		SceneManager.LoadScene(1);
	}

	public void ComparingSpectraVisualization()
	{
		SceneManager.LoadScene(2);
	}
}
