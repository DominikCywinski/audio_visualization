﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Audio;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine.Assertions.Must;
using UnityEngine.Audio;
using Debug = UnityEngine.Debug;

[RequireComponent(typeof(AudioSource))]

public class Audio : MonoBehaviour
{
	private static Action OnUpdate;
	private AudioSource _audioSource;
	public float[] _samples;
	public float[] _frequencyBands;
	public AudioMixerGroup _mixerGroupMute, _mixerGroupMaster;
	public float[] _scale;
	public bool _isPaused;

	protected static Audio instance;

	void Start()
	{
		_samples = new float[512];
		_frequencyBands = new float[8];
		_audioSource = GetComponent<AudioSource>();
		_scale = new float[512];
		_isPaused = false;

		if (instance == null)
		{
			instance = this;
		}

		OnUpdate += CallOnUpdate;
	}

	void Update()
	{
		if (this == instance)
		{
			OnUpdate?.Invoke();
		}
	}
	
	private void CallOnUpdate()
	{
		if (!_isPaused)
		{
			GetSpectrumAudioSource();
			MakeFrequencyBands();
		}
	}

	private void OnDestroy()
	{
		OnUpdate -= CallOnUpdate;

		if (instance == this)
		{
			instance = null;
		}
	}

	void GetSpectrumAudioSource()
	{
		_audioSource.GetSpectrumData(_samples, 0, FFTWindow.Blackman);
	}

	void MakeFrequencyBands()
	{
		int count = 0;

		for (int i = 0; i < 8; i++)
		{
			float average = 0;
			int sampleCount = (int)Mathf.Pow(2, i) * 2; //multiply by 2 to avoid 2^0=1		

			if (i == 7)
			{
				sampleCount += 2; //+2 to add 2 last samples.
			}
			for (int j = 0; j < sampleCount; j++)
			{
				average += _samples[count] * (count + 1); //(count +1 ) to increase the values of high frequency // +1 to avoid multiply by zero
				count++;
			}
			average /= sampleCount;
			_frequencyBands[i] = average * 10; //multiply by 10 to increase small average
		}
	}

	/*
	 * song has 22050 Hz and 512 samples = 43 Hz per sample
	 * 1 band -> 2 samples * 43 Hz = 86 Hz
	 * 2 band -> 4 samples *43 Hz = 172 Hz -> 87 Hz to 258 Hz band
	 * 3 band -> 8 samples *43 Hz = 344 Hz -> 259 Hz to 602 Hz band
	 * 4 band -> 16 samples *43 Hz = 688 Hz -> 603 Hz to 1290 Hz band
	 * 5 band -> 32 samples *43 Hz = 1376 Hz -> 1291 Hz to 2666 Hz band
	 * 6 band -> 64 samples *43 Hz = 2752 Hz -> 2667 Hz to 5418 Hz band
	 * 7 band -> 128 samples *43 Hz = 5504 Hz -> 5419 Hz to 10922 Hz band
	 * 8 band -> 256 samples *43 Hz = 11008 Hz -> 10923 Hz to 21930 Hz band
	 * summary 510 samples so need to add 2 last samples.
	 */
	public void PauseSong()
	{
		_audioSource.Pause();
		_isPaused = true;
	}

	public void PlaySong()
	{
		_audioSource.Play();
		_isPaused = false;
	}

	public void MuteSong()
	{
		_audioSource.outputAudioMixerGroup = _mixerGroupMute;
	}

	public void UnMuteSong()
	{
		_audioSource.outputAudioMixerGroup = _mixerGroupMaster;
	}
}