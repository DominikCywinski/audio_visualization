﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class Settings : MonoBehaviour
{
	public TMP_Dropdown _resolutionDropdown;
	Resolution[] _resolutions;

	private void Start()
	{
		_resolutions = Screen.resolutions;
		_resolutionDropdown.ClearOptions();

		List<string> options = new List<string>();

		int currentResolutionIndex = 0;

		for(int i = 0; i< _resolutions.Length; i++)
		{
			string option = _resolutions[i].width + " x " + _resolutions[i].height;
			options.Add(option);

			if(_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
			{
				currentResolutionIndex = i;
			}
		}

		_resolutionDropdown.AddOptions(options);
		_resolutionDropdown.value = currentResolutionIndex;
		_resolutionDropdown.RefreshShownValue();
	}

	public void SetResolution(int resoultionIndex)
	{
		Resolution resolution = _resolutions[resoultionIndex];
		Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
	}

	public void SetFullScreen (bool isFull)
	{
		Screen.fullScreen = isFull;
	}
}
