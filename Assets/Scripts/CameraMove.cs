﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
	public float _wheelSpeed;
	public float _movingSpeed;
	public float _maxHeight;

	public Transform _target;

	[SerializeField]
	public GameObject player;

	[SerializeField]
	private float _sensitivityX = 1f, _sensitivityY = 1f;

	private bool disableCameraControl;

	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	void Update()
	{
		ShowCursor();
		CameraMoving();
		CameraUpDown();
		HandleLookX();
		HandleLookY();
	}
	void ShowCursor()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			disableCameraControl = !disableCameraControl;
			Cursor.lockState = disableCameraControl ? CursorLockMode.None : CursorLockMode.Locked;
		}

		Cursor.visible = disableCameraControl ? true : false;
		
		if (disableCameraControl)
		{
			return;
		}
	}
	void CameraMoving()
	{
		float horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * _movingSpeed;
		float vertical = Input.GetAxis("Vertical") * Time.deltaTime * _movingSpeed;
		player.transform.Translate(horizontal, 0, vertical);
	}

	void CameraUpDown()
	{
		transform.LookAt(_target);
		if (player.transform.position.y > 0 && player.transform.position.y < _maxHeight)
		{
			player.transform.Translate(0, Input.GetAxis("Mouse ScrollWheel") * _wheelSpeed, 0, Space.Self);
		}
		else if (player.transform.position.y <= 0 && Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			player.transform.Translate(0, Input.GetAxis("Mouse ScrollWheel") * _wheelSpeed, 0, Space.Self);
		}
		else if (player.transform.position.y >= _maxHeight && Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			player.transform.Translate(0, Input.GetAxis("Mouse ScrollWheel") * _wheelSpeed, 0, Space.Self);
		}
	}

	private void HandleLookX()
	{
		float mouseX = Input.GetAxis("Mouse X");
		Vector3 rotation = player.transform.localEulerAngles;
		rotation.y += mouseX * _sensitivityX;
		player.transform.localEulerAngles = rotation;
	}

	private void HandleLookY()
	{
		float mouseY = -Input.GetAxis("Mouse Y");
		Vector3 rotation = transform.localEulerAngles;
		rotation.x += mouseY * _sensitivityY;
		rotation.x = (rotation.x > 180) ? rotation.x - 360 : rotation.x;
		rotation.x = Mathf.Clamp(rotation.x, -70, 60);
		transform.localEulerAngles = rotation;
	}
}