﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteAudioSource : MonoBehaviour
{
	public Audio _audioSource1, _audioSource2;
	bool _isMuted = false;

	void Start()
	{
	}

	void Update()
	{
		MuteSong();
		PausePlaySong();
	}

	public void PausePlaySong()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			if(_audioSource1._isPaused)
			{
				_audioSource1.PlaySong();
			}
			else
			{
				_audioSource1.PauseSong();
			}

			if(_audioSource1!=_audioSource2)
			{
				if (_audioSource2._isPaused)
				{
					_audioSource2.PlaySong();
				}
				else
				{
					_audioSource2.PauseSong();
				}
			}
		}
	}

	public void MuteSong()
	{
		if (Input.GetKeyDown(KeyCode.M))
		{
			if (!_isMuted)
			{
				_audioSource1.MuteSong();
				_audioSource2.MuteSong();
			}
			else
			{
				_audioSource1.UnMuteSong();
				_audioSource2.UnMuteSong();
			}
			_isMuted = !_isMuted;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha1) && _audioSource1!=_audioSource2)
		{
			_audioSource1.UnMuteSong();
			_audioSource2.MuteSong();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2) && _audioSource1 != _audioSource2)
		{
			_audioSource2.UnMuteSong();
			_audioSource1.MuteSong();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3) && _audioSource1 != _audioSource2)
		{
			_audioSource2.UnMuteSong();
			_audioSource1.UnMuteSong();
		}
	}
}
