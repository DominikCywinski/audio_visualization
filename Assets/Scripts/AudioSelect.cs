﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AudioSelect : MonoBehaviour
{
	public TMP_Dropdown _dropDown;
	public TMP_Dropdown _dropDown2;
	public string _fileDirectory;
	public static string _selectedSong1;
	public static string _selectedSong2;
	private static int _currentSong1Index = 2;
	private static int _currentSong2Index = 3;
	public static bool _useMicrophone1;
	public static bool _useMicrophone2;

	List<string> titles = new List<string> { };

	private void Start()
	{
		ShowSongsList();
		SetSelectedSong(_currentSong1Index);
		SetSelectedSong2(_currentSong2Index);
	}

	void ShowSongsList()
	{
		_fileDirectory = Application.streamingAssetsPath + "/Audio";//Application.streamingAssetsPath + "/Audio";
		string[] files = Directory.GetFiles(_fileDirectory);

		for (int i = 0; i < files.Length; i++)
		{
			if (files[i].EndsWith(".mp3") || files[i].EndsWith(".wav"))
			{
				titles.Add(files[i].Remove(0, _fileDirectory.Length + 1)); //remove path
			}
		}

		if (Microphone.devices.Length > 0)
		{
			titles.Insert(0, "Use microphone: " + Microphone.devices[0].ToString());
		}

		titles.Insert(0, "Choose song...");
		_dropDown.AddOptions(titles);
		_dropDown2.AddOptions(titles);
	}

	public void SetSelectedSong(int index)
	{
		if (index == 1)
		{
			_useMicrophone1 = true;
		}
		else
		{
			_selectedSong1 = titles[index];
			_useMicrophone1 = false;
		}
		
		_currentSong1Index = index;
		_dropDown.value = _currentSong1Index;
		_dropDown.RefreshShownValue();
	}
	public void SetSelectedSong2(int index)
	{
		if(index==1)
		{
			_useMicrophone2 = true;
		}
		else
		{
			_selectedSong2 = titles[index];
			_useMicrophone2 = false;
		}
		
		_currentSong2Index = index;
		_dropDown2.value = _currentSong2Index;
		_dropDown2.RefreshShownValue();
	}
}
