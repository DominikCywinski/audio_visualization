﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class ParametricCube : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Transform Cube;
	public Gradient Gradient;
	public float Sample;
	public event Action<ParametricCube> OnSelected;
	public event Action<ParametricCube> OnDeselected;

	private MaterialPropertyBlock propertyBlock;
	private MeshRenderer meshRenderer;

	private void Awake()
	{
		propertyBlock = new MaterialPropertyBlock();
		meshRenderer = Cube.GetComponent<MeshRenderer>();
	}

	public void SetValue(float sample, float value, float maxScale, float maxValue)
	{
		SetCubeScale(new Vector3(Cube.localScale.x, value * maxScale, Cube.localScale.z));

		Sample = sample;

		meshRenderer.GetPropertyBlock(propertyBlock);
		propertyBlock.SetColor("_Color", Gradient.Evaluate(Mathf.InverseLerp(0, maxValue, value)));
		meshRenderer.SetPropertyBlock(propertyBlock);
	}

	public void SetCubeScale(Vector3 scale)
	{
		Cube.localScale = scale;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		OnSelected?.Invoke(this);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		OnSelected?.Invoke(this);
	}
}