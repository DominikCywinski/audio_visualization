﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class AverageIn8cubes : MonoBehaviour
{
	public Audio _audio;
	public int _band;
	public float _startScale;
	public float _scaleMultiplier;

	private ParametricCube _parametricCube;

	private void Awake()
	{
		_parametricCube = GetComponent<ParametricCube>();
	}

	void Update()
	{
		_parametricCube.SetCubeScale(new Vector3(1, (_audio._frequencyBands[_band] * _scaleMultiplier) + _startScale, 1));
	}
}
