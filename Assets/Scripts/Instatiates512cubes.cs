﻿using UnityEngine;
using TMPro;

public class Instatiates512cubes : MonoBehaviour
{
	public GameObject _sampleCubePrefab;
	public ParametricCube[] _sampleParametricCube = new ParametricCube[512];
	public float _maxScale;
	public float radius;
	public Audio _audio;
	public TextMeshProUGUI _sampleValueText;

	void Start()
	{
		for (int i = 0; i < 512; i++)
		{
			ParametricCube _instanceSampleCube = Instantiate(_sampleCubePrefab).GetComponent<ParametricCube>(); //clone from template
			_instanceSampleCube.transform.position = this.transform.position;
			_instanceSampleCube.transform.parent = this.transform;
			_instanceSampleCube.name = "sample No: " + i; //give name
			this.transform.eulerAngles = new Vector3(0, -0.703125f * i, 0); //rotate in 3D - 360 divided by 512 = 0.703125 
			_instanceSampleCube.transform.position = new Vector3(this.transform.position.x, 0, transform.position.z + radius);

			_instanceSampleCube.OnSelected += OnCubeSelected;

			_sampleParametricCube[i] = _instanceSampleCube;
		}
	}

	private void OnCubeSelected(ParametricCube cube)
	{
		if (cube.Sample > 0)
		{
			_sampleValueText.text = cube.name + "\nvalue: " + cube.Sample.ToString().Replace(",", ".")
			+ "\n	 " + (20 * Mathf.Log(Mathf.Abs(cube.Sample), 10.0f)).ToString().Replace(",", ".") + " dB";
		}
		else
		{
			_sampleValueText.text = cube.name + "\nvalue: " + cube.Sample.ToString().Replace(",", ".");
		}
	}
	
	protected virtual void Update()
	{
		for (int i = 0; i < 512; i++)
		{
			if (_audio._samples[i] != 0)
			{
				_audio._scale[i] = Mathf.Log(_audio._samples[i], 10.0f) - Mathf.Log(GetMinValue(), 10.0f); 
			}
			else
			{
				_audio._scale[i] = 0;
			}
			_sampleParametricCube[i].SetValue(_audio._samples[i], _audio._scale[i], _maxScale, GetMaxValue());
		}
	}

	float GetMaxValue()
	{
		float temp_max = _audio._scale[0];

		for (int i = 0; i < 512; i++)
		{
			if (_audio._scale[i] > temp_max)
			{
				temp_max = _audio._scale[i];
			}
		}
		return temp_max;
	}

	float GetMinValue()
	{
		float temp_min = _audio._samples[0];

		for (int i = 1; i < 512; i++)
		{
			if (_audio._samples[i] != 0 && _audio._samples[i] < temp_min)
			{
				temp_min = _audio._samples[i];
			}
		}
		return temp_min;
	}
}