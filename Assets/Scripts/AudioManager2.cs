﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AudioManager2 : AudioManager
{
	protected override void Start()
	{
		_Source = GetComponent<AudioSource>();
		PlayAudio();
	}

	void PlayAudio()
	{
		if (!AudioSelect._useMicrophone2)
		{
			_clipName = AudioSelect._selectedSong2;
			_FileDirectory = Application.streamingAssetsPath + "/Audio";

			files = Directory.GetFiles(_FileDirectory);

			for (int i = 0; i < files.Length; i++)
			{
				if (files[i].EndsWith(_clipName))
				{
					if (files[i].EndsWith(".mp3"))
					{
						_Source.clip = new WWW(files[i]).GetAudioClip(false, true, AudioType.MPEG);
					}
					else
					{
						_Source.clip = new WWW(files[i]).GetAudioClip(false, true, AudioType.WAV);
					}
				}
			}
		}
		else
		{
			_Source.clip = Microphone.Start(Microphone.devices[0].ToString(), true, 10, AudioSettings.outputSampleRate);
		}
		_Source.Play();
	}
}
